#pragma once

#include "image.h"
#include <stdio.h>

enum read_status {
    READ_OK,
    READ_IMAGE_NULL,
    READ_FILE_CLOSED,
    READ_EOF,
    READ_INVALID_HEADER
};

enum read_status read_image(const char* path, struct image* img);

enum write_status {
    WRITE_OK,
    WRITE_IMAGE_NULL = 0xF, // чтобы коды ошибок отличались для чтения/записи
    WRITE_EOF,
    WRITE_FILE_CLOSED
};

enum write_status write_image(const char* path, struct image* img);
