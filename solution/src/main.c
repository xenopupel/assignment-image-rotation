#include <stdio.h>

#include "image_io.h"
#include "rotate.h"

int main(int argc, char** argv) {
    if (argc != 3) {
        printf("This program takes two arguments!");
        return -1;
    }
    struct image source;
    enum read_status read_status = read_image(argv[1], &source);
    if (read_status == READ_OK) {
        printf("Read success...\n");
        struct image rotated = rotate(source);
        free(source.data);
        enum write_status write_status = write_image(argv[2], &rotated);
        free(rotated.data);
        if (write_status == WRITE_OK) {
            printf("Success!\n");
            return 0;
        } else {
            printf("Write error: %d\n", write_status);
            return write_status;
        }
    } else {
        printf("Read error: %d\n", read_status);
        return read_status;
    }
}
