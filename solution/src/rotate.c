#include "rotate.h"

struct image rotate( struct image const source ) {
    struct image img = (struct image) {
        .width = source.height,
        .height = source.width,
        .data = (struct pixel*) malloc(sizeof(struct pixel) * source.width * source.height)
    };
    size_t p = 0;
    for (size_t col = img.width - 1; col + 1 > 0; col--) {
        for (size_t row = 0; row < img.height; row++) {
            img.data[row * img.width + col] = source.data[p++];
        }
    }
    return img;
}
