#include "image_io.h"
#include "bmp_format.h"

enum read_status read_image(const char* path, struct image* img) {
    FILE* fin = fopen(path, "r");
    if (!img) return READ_IMAGE_NULL;
    if (!fin) return READ_FILE_CLOSED;
    enum read_status read_status = from_bmp(fin, img);
    fclose(fin);
    return read_status;
}

enum write_status write_image(const char* path, struct image* img) {
    FILE* fout = fopen(path, "w");
    if (!img) return WRITE_IMAGE_NULL;
    if (!fout) return WRITE_FILE_CLOSED;
    enum write_status write_status = to_bmp(fout, img);
    fclose(fout);
    return write_status;
}
